## The Impact of View Histories on Edit Recommendations ##
Abstract—Recommendation systems are intended to increase developer productivity by recommending files to edit. These systems mine association rules in software revision histories. However, mining coarse-grained rules using only edit histories produces recommendations with low accuracy, and can only produce recommendations after a developer edits a file. In this work, we explore the use of finer-grained association rules, based on the insight that view histories help characterize the contexts of files to edit. To leverage this additional context and fine-grained association rules, we have developed MI, a recommendation system extending ROSE, an existing edit-based recommendation system. We then conducted a comparative simulation of ROSE and MI using the interaction histories stored in the Eclipse Bugzilla system. The simulation demonstrates that MI predicts the files to edit with significantly higher recommendation accuracy than ROSE (about 63 over 35 percent), and makes recommendations earlier, often before developers begin editing. Our results clearly demonstrate the value of considering both views and edits in systems to recommend files to edit, and results in more accurate, earlier, and more flexible recommendations.

### Publication ###
Seonah Lee; Sungwon Kang; Sunghun Kim; Staats, M., "The Impact of View Histories on Edit Recommendations," Software Engineering, IEEE Transactions on , vol.41, no.3, pp.314,330, March 1 2015 
 
### Data for the Experiment ###
You can download the data from http://salab.kaist.ac.kr/tse2015/AllProjects.zip
 
### Code for the Experiment ###
You can access the code in http://bitbucket.org/saleese/mi-ve