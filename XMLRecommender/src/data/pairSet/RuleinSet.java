package data.pairSet;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import data.queue.Event;

public class RuleinSet {

	String fileName;
	Set<String> tmpSet; 
	Set<String> antecedentSet = new LinkedHashSet<String>();
	Set<String> consequentSet = new LinkedHashSet<String>();

	//	int iSimilarContext = 0;
	//	Set<String> contextSet = new LinkedHashSet<String>();

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	//	public Set<String> getContextSet() {
	//		return contextSet;
	//	}
	//
	//	public void setContextSet(Set<String> similarContextSet) {
	//		this.contextSet = similarContextSet;
	//	}

	public void addtoPrev(String c) {
		antecedentSet.add(c);
	}

	public void addtoNext(String c) {
		consequentSet.add(c);
	}

	public void addAlltoPrev(Set<String> c) {
		antecedentSet.addAll(c);
	}

	public void addAlltoNext(Set<String> c) {
		consequentSet.addAll(c);
	}

	public void addAll(RuleinSet rule) {
		this.addAlltoPrev(rule.getPrev());
		this.addAlltoNext(rule.getNext());		
	}

	//	boolean containinPrev(String c) {
	//		return antecedentSet.contains(c);
	//	}


	//	void printContext() {
	//		if (contextSet == null ) return ;
	//		if (contextSet.size() == 0) return ;
	//
	//		for (String element: contextSet) {
	//			System.out.println(element);
	//		}		
	//	}

	//	int countinPrev(Set<String> currentContextSet) {
	//	if (currentContextSet == null ) return -1;
	//	if (currentContextSet.size() == 0) return 0;
	//	
	//	int i = 0;
	//	for (String element: currentContextSet) {
	//		if (containinPrev(element)) {
	//			i++;
	//			contextSet.add(element);
	//		}
	//	}
	//	
	//	setiSimilarContext(i);
	//	return i;
	//}

	public boolean containinPrev(Set<String> currentContextSet) {
		if (currentContextSet.size() == 0)
			return false;

		for (String element: currentContextSet) {
			if (antecedentSet.contains(element))
				continue;
			else
				return false;
		}

		return true;
	}
	
	public boolean containinPrev(Set<String> currentContextSet, int k) {
		
		if (k == 0) return true;		
//		if (currentContextSet.size() == 0)
//			return true;
		
		tmpSet = new LinkedHashSet<String>();

		int count = 0;
		
		for (String element: currentContextSet) {
			if (antecedentSet.contains(element)) {
				tmpSet.add(element);				
				count++;				
				if (count >= k) return true;
				
				continue;
			}
			else
				return false;
		}

		return true;
	}
	
	
	public Set<String> getinPrev() {
		return tmpSet;
	}
	
	public void clearinPrev() {
		tmpSet.clear();
	}

	public boolean containinPrev(String c) {
		return antecedentSet.contains(c);
	}

	public boolean containinNext(Set<String> currentContextSet) {
		if (currentContextSet.size() == 0)
			return false;

		for (String element: currentContextSet) {
			if (consequentSet.contains(element))
				continue;
			else
				return false;
		}

		return true;
	}	


	public boolean containinNext(String c) {
		return consequentSet.contains(c);
	}

	public Set<String> getPrev() {
		return antecedentSet;
	}

	public Set<String> getNext() {
		return consequentSet;
	}

	//	public int getiSimilarContext() {
	//		return iSimilarContext;
	//	}
	//
	//	public void setiSimilarContext(int iSimilarContext) {
	//		this.iSimilarContext = iSimilarContext;
	//	}

	public void print() {		
		System.out.println("-- rule --");	
		//		if (antecedentSet != null) {
		//			for (String c: antecedentSet) {
		//				System.out.println("prev: " + c);				
		//			}
		//		}
		System.out.print("--> ");
		if (consequentSet != null) {
			for (String c: consequentSet) {
				System.out.println("next: " + c);				
			}
		}	
	}

	public void clear() {
		antecedentSet.clear();
		consequentSet.clear();
		//		iSimilarContext = 0;
		//		contextSet.clear();
	}

	public void clearContext() {
		//		iSimilarContext = 0;
		//		contextSet.clear();
	}

	public int size() {
		return consequentSet.size();
	}


	public boolean containin(Set<Event> currentContextSet) {

		for (Event event: currentContextSet) {
			if (event.getAction().equals("visit")){
				if (antecedentSet.contains(event.getElement()))
					continue;				
			}
			else if (event.getAction().equals("edit")){
				if (consequentSet.contains(event.getElement()))
					continue;
			}
			else {
				return false;
			}
		}

		return true;
	}
	
	// added...
	public boolean containinPrev(Set<String> currentContextSet, double percentage) {
		if (currentContextSet.size() == 0)
			return true;

		Set<String> intersectionSet = intersection(antecedentSet, currentContextSet);
		Set<String> unionSet = union(antecedentSet, currentContextSet);
		
		double value = ((double) intersectionSet.size()) / unionSet.size();
		
		if (value >= percentage)
			return true;
		else
			return false;		
	}

	public Set<String> intersection(Set<String> setA, Set<String> setB) {
		Set<String> tmp = new TreeSet<String>();
		for (String x : setA)
			if (setB.contains(x))
				tmp.add(x);
		return tmp;
	}

	public Set<String> union(Set<String> setA, Set<String> setB) {
		Set<String> tmp = new TreeSet<String>(setA);
		tmp.addAll(setB);
		return tmp;
	}
}
