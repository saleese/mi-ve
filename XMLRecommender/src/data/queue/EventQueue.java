package data.queue;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class EventQueue {

	LinkedList<Event> eventQueue = new LinkedList<Event>();
	int n;

	public EventQueue(int n) {
		this.n = n;
	}

	public boolean add(String element,  String action) {
		return add(new Event(element, action));
	}

	public boolean add(Event event) {
		boolean result = eventQueue.add(event);

		if (eventQueue.size() > n) {
			eventQueue.removeFirst();
		}

		return result;
	}

	public void print() {
		System.out.print("[In Queue] " );
		for (Event event: eventQueue) {
			System.out.print(event.getAction()+ ":" + event.getElement() + ", " );			
		}
		System.out.println();
	}	

	public Set<Event> toSet() {
		Set<Event> eventSet = new LinkedHashSet<Event> ();

		eventSet.addAll(eventQueue);

		return eventSet;
	}

	public int size() {
		return eventQueue.size();
	}
	
	public void clear() {
		eventQueue.clear();
	}

}
