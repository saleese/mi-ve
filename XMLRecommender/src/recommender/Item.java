package recommender;

public class Item {
	String element;
	int iCount;
	double dConfidence;
	
	public Item(String pElement, int count) {
		element = pElement;
		iCount = count;
	}
	
	public void increaseCount() {
		iCount = iCount +1;
	}
	
	public String getElement() {
		return element;
	}

	public void setElement(String pElement) {
		this.element = pElement;
	}

	public int getCount() {
		return iCount;
	}

	public void setCount(int iCount) {
		this.iCount = iCount;
	}
	
	public double getConfidence() {
		return dConfidence;
	}

	public void setConfidence(double dConfidence) {
		this.dConfidence = dConfidence;
	}
	
	
}
