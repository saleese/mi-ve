package recommender;
import java.util.Comparator;

  public class ItemComparator implements Comparator<Item> {

	@Override
	public int compare(Item o1, Item o2) {
		if (o1.iCount > o2.iCount)
			return -1;
		else if (o1.iCount < o2.iCount) 
			return 1;
		else
			return 0;	
	}
  }