package recommender;

import java.io.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import recommender.Item;

public class ItemList {

	List<Item> itemList = new LinkedList<Item>();

	public void add(Item item) {
		itemList.add(item);
	}

	public void add(String sLine) {
		if (sLine == null)
			return;

		if (this.contains(sLine)) {
			increaseNoList(sLine);
		} else {
			itemList.add(new Item(sLine, 1));
		}
	}

	public void addAll(Collection<String> iSet) {
		for (String element : iSet) {
			this.add(element);
		}
	}

	public void addAll(List<Item> itemList) {
		for (Item element : itemList) {
			this.add(element);
		}
	}

	public void calculate_confidence(double queryCount) {
		for (Item item : itemList) {
			item.setConfidence(item.getCount() / queryCount);
		}
	}

	public void clear() {
		itemList.clear();
	}

	public boolean contains(String element) {
		for (Item item : itemList) {
			if (item.getElement().equals(element)) {
				return true;
			}
		}

		return false;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	public LinkedList<String> getItemList(int iThreshold) {
		LinkedList<String> stringList = new LinkedList<String>();

		for (Item item : itemList) {
			if (item.getCount() >= iThreshold) {
				stringList.add(item.getElement());
			}
		}

		return stringList;
		// for (Item item: garbageList) {
		// itemList.remove(item);
		// }
	}

	public void increaseNoList(String sLine) {
		Iterator iterator;

		// System.out.println(sLine);
		if (itemList.isEmpty())
			return;
		iterator = itemList.iterator();
		while (iterator.hasNext()) {
			Item item = (Item) iterator.next();

			// System.out.println(item.sProgramElement);
			if (item.getElement().equals(sLine))
				item.increaseCount();
		}
	}

	public boolean isEmpty() {
		return itemList.isEmpty();
	}

	public boolean isFrequent(String element, int iThreshold) {
		for (Item item : itemList) {
			if (item.getElement().equals(element)) {
				if (item.getCount() >= iThreshold)
					return true;
			}
		}

		return false;
	}

	public int likelihood(int N) {
		if (itemList.isEmpty())
			return -1;

		int likelihood = 0;
		for (int i = 0; i < 3; i++) {
			if (itemList.size() <= i)
				break;

			likelihood = likelihood + itemList.get(i).getCount();
		}

		return likelihood / 3;
	}

	private void log(Object aObject) {
		System.out.println(String.valueOf(aObject));
	}

	public void print(int iThreshold) {
		Iterator iterator;

		// if (itemList.isEmpty())
		// return;

		System.out
				.println("The number of program elements: " + itemList.size());
		iterator = itemList.iterator();
		while (iterator.hasNext()) {
			Item item = (Item) iterator.next();

			if (item.getCount() >= iThreshold)
				System.out.println(item.getCount() + ": " + item.getElement());
		}
	}

	public Set<Item> recommend(int N) {
		if (itemList.isEmpty())
			return null;

		Set<Item> recommendationSet = new LinkedHashSet<Item>();

		for (int i = 0; i < N; i++) {
			if (itemList.size() <= i)
				break;

			// System.out.println("recommended one:" +
			// itemList.get(i).getCount());
			Item item2recommend = itemList.get(i);
			if ((item2recommend.getCount() < 1)
					&& (item2recommend.getConfidence() < 0.1))
				break;

			recommendationSet.add(itemList.get(i));
		}

		return recommendationSet;
	}
	
	public void printRecommendedItems(int N) {
		if (itemList.isEmpty())
			return ;

		Set<Item> recommendationSet = new LinkedHashSet<Item>();

		for (int i = 0; i < N; i++) {
			if (itemList.size() <= i)
				break;

			// System.out.println("recommended one:" +
			// itemList.get(i).getCount());
			Item item2recommend = itemList.get(i);
			if ((item2recommend.getCount() < 1)
					&& (item2recommend.getConfidence() < 0.1))
				break;

			System.out.println("- " + itemList.get(i).getElement());
		}

		return ;
	}

	public void remove(String element) {
		List<Item> garbageList = new LinkedList<Item>();

		for (Item item : itemList) {
			if (item.getElement().equals(element)) {
				garbageList.add(item);
			}
		}

		for (Item item : garbageList) {
			itemList.remove(item);
		}
	}

	public void remove_min_confidence(double confidence) { // �հ� �̻�ȳ...
		if (itemList.isEmpty())
			return;

		Iterator iterator = itemList.iterator();
		int i = 0;
		while (iterator.hasNext()) {
			i++;
			Item item = (Item) iterator.next();

			if (item.dConfidence < confidence)
				iterator.remove();
		}
	}

	public void remove_min_support(int support) {
		if (itemList.isEmpty())
			return;

		Iterator iterator = itemList.iterator();
		int i = 0;
		while (iterator.hasNext()) {
			i++;
			Item item = (Item) iterator.next();

			if (item.iCount < support)
				iterator.remove();
		}
	}

	public Item removeFirst() {
		// error:
		return ((LinkedList<Item>) itemList).removeFirst();
	}

	public Set<String> returnTop(int N) {
		Set<String> returnSet = new LinkedHashSet<String>();

		if (itemList.isEmpty())
			return null;

		System.out.println("The most Frequent Elements in " + itemList.size());
		Iterator iterator = itemList.iterator();

		int i = 0;
		while (iterator.hasNext()) {
			i++;
			Item item = (Item) iterator.next();

			// if (item.iCount >= iThreshhold)
			System.out.println(item.getCount() + ": " + item.getElement());
			returnSet.add(item.getElement());

			if (i >= N)
				break;
		}

		return returnSet;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

	public int size() {
		return itemList.size();
	}

	public void sort() {
		ItemComparator ic = new ItemComparator();
		Collections.sort(itemList, ic);
	}

	public void sort2Confidence() {
		ConfidenceComparator ic = new ConfidenceComparator();
		Collections.sort(itemList, ic);
	}

}
