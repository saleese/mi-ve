package recommender
;

import java.io.FileOutputStream;
import java.io.PrintStream;

import recommender.revision.file.XMLRecommenderFile_EarlyR;
import recommender.revision.file.XMLRecommenderFile_AccurateR;
import recommender.revision.file.XMLRecommenderFile_FlexibleR;
import recommender.revision.hybrid.XMLRecommenderHybrid;
import recommender.revision.method.XMLRecommenderMethod;


public class Recommender {

//	static String directory = "D:/MylynData/2011-07-30-mylyn/Project_Platform";
	static String directory = "D:/MylynData/2011-07-30-mylyn/AllProjects";
//	static String edit_directory = "D:/MylynData/2011-07-30-mylyn-edit/Project_All_Edit";

	public static void main(String argv[]) {

		FileOutputStream output;
		PrintStream outfile;
		try
		{     
			output = new FileOutputStream("out.txt");
			outfile = new PrintStream(output);

//			for (int i = 1; i <= 1; i++) {
				XMLRecommenderFile_FlexibleR recommender = new XMLRecommenderFile_FlexibleR(outfile);
//				XMLRecommenderFile_EarlyR recommender = new XMLRecommenderFile_EarlyR(outfile);
//				XMLRecommenderFile_AccurateR recommender = new XMLRecommenderFile_AccurateR(outfile);
				recommender.run(directory);
//			}
		}        
		catch(Exception e)
		{
			System.out.println("Could not load file!");
		}
	}
}
