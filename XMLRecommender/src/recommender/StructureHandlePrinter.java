package recommender;

public class StructureHandlePrinter {
	static String previousProgramMethod = null;
	
	public void printProgramElement(String sProgramElement) {
		
		String currentProgramMethod = new String();
		
		if (sProgramElement == null) 
			return;
		
		if (!sProgramElement.contains("src"))
			return;

		if (sProgramElement.contains("{")) // package
//			currentProgramMethod = currentProgramMethod.concat(sProgramElement.substring(sProgramElement.indexOf('<') + 1, sProgramElement.indexOf('{')) + ", ");
//			System.out.print(sProgramElement.substring(sProgramElement.indexOf('<') + 1, sProgramElement.indexOf('{')) + ", ");

		if (sProgramElement.contains("[")) // file
//			currentProgramMethod = currentProgramMethod.concat(sProgramElement.substring(sProgramElement.indexOf('{') + 1, sProgramElement.indexOf('[')) + ", ");
//			System.out.print(sProgramElement.substring(sProgramElement.indexOf('{') + 1, sProgramElement.indexOf('[')) + ", ");
		
		if (sProgramElement.contains("[")) { // class
			String sClass = sProgramElement.substring(sProgramElement.indexOf('[') +1);
			
			if (sClass.contains("~"))
				sClass = sClass.substring(0, sClass.indexOf('~'));
			
			if (sClass.contains("^"))
				sClass = sClass.substring(0, sClass.indexOf('^'));

			currentProgramMethod = currentProgramMethod.concat(sClass + ", ");
//			System.out.print(sClass + ", ");
		}
		
		if (sProgramElement.contains("~")) { // method
			String sMethod =  sProgramElement.substring(sProgramElement.indexOf('~') + 1);
			
			if (sMethod.contains("~"))
				sMethod = sMethod.substring(0, sMethod.indexOf('~'));
			
			currentProgramMethod = currentProgramMethod.concat(sMethod);
//			System.out.print(sMethod);
		}		
		if (sProgramElement.contains("^")) // field
			currentProgramMethod = currentProgramMethod.concat(sProgramElement.substring(sProgramElement.indexOf('^') + 1));
//			System.out.print(sProgramElement.substring(sProgramElement.indexOf('^') + 1));
		
		if (!currentProgramMethod.equals(previousProgramMethod))
			System.out.println(currentProgramMethod);
		previousProgramMethod = currentProgramMethod;
	}
	
	public String toElement(String sProgramElement) {
		
//		String currentProgramMethod = new String();
		
		if (sProgramElement == null) 
			return "*";
		
		if (sProgramElement.contains("{")) 
			sProgramElement = sProgramElement.substring(sProgramElement.lastIndexOf("{"));
		
		if (sProgramElement.contains("/")) 
			sProgramElement = sProgramElement.substring(sProgramElement.lastIndexOf("/"));
			

//		if (sProgramElement.contains("{")) // package
////			currentProgramMethod = currentProgramMethod.concat(sProgramElement.substring(sProgramElement.indexOf('<') + 1, sProgramElement.indexOf('{')) + ", ");
////			System.out.print(sProgramElement.substring(sProgramElement.indexOf('<') + 1, sProgramElement.indexOf('{')) + ", ");
//
//		if (sProgramElement.contains("[")) // file
////			currentProgramMethod = currentProgramMethod.concat(sProgramElement.substring(sProgramElement.indexOf('{') + 1, sProgramElement.indexOf('[')) + ", ");
////			System.out.print(sProgramElement.substring(sProgramElement.indexOf('{') + 1, sProgramElement.indexOf('[')) + ", ");
//		
//		if (sProgramElement.contains("[")) { // class
//			String sClass = sProgramElement.substring(sProgramElement.indexOf('[') +1);
//			
//			if (sClass.contains("~"))
//				sClass = sClass.substring(0, sClass.indexOf('~'));
//			
//			if (sClass.contains("^"))
//				sClass = sClass.substring(0, sClass.indexOf('^'));
//
//			currentProgramMethod = currentProgramMethod.concat(sClass + ", ");
////			System.out.print(sClass + ", ");
//		}
//		
//		if (sProgramElement.contains("~")) { // method
//			String sMethod =  sProgramElement.substring(sProgramElement.indexOf('~') + 1);
//			
//			if (sMethod.contains("~"))
//				sMethod = sMethod.substring(0, sMethod.indexOf('~'));
//			
//			currentProgramMethod = currentProgramMethod.concat(sMethod);
////			System.out.print(sMethod);
//		}		
//		if (sProgramElement.contains("^")) // field
//			currentProgramMethod = currentProgramMethod.concat(sProgramElement.substring(sProgramElement.indexOf('^') + 1));
////			System.out.print(sProgramElement.substring(sProgramElement.indexOf('^') + 1));
		
		
		return sProgramElement;
	}

	public String toFile(String sProgramElement) {
		sProgramElement = this.toElement(sProgramElement);
		
		if (sProgramElement.contains("[")) {
			sProgramElement = sProgramElement.substring(0, sProgramElement.indexOf('['));
		}
		return sProgramElement;
	}
	
	
}
