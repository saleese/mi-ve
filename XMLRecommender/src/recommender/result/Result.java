package recommender.result;

import java.io.PrintStream;
import java.util.Set;

import data.pairSet.RuleinSet;

import recommender.Item;
import recommender.ItemList;

public class Result {

	private double precision10;
	private double recall10;
	int Nth;
	long sum_true = 0;
	long sum_false = 0;
	
	public long getSum_true() {
		return sum_true;
	}

	public void setSum_true(long sum_true) {
		this.sum_true = sum_true;
	}

	public long getSum_false() {
		return sum_false;
	}

	public void setSum_false(long sum_false) {
		this.sum_false = sum_false;
	}



	public int getNth() {
		return Nth;
	}

	public void setNth(int nth) {
		Nth = nth;
	}

	public double getPrecision() {
		return precision10;
	}

	public void setPrecision(double precision) {
		this.precision10 = precision;
	}

	public double getRecall() {
		return recall10;
	}

	public void setRecall(double recall10) {
		this.recall10 = recall10;
	}

	public void checkResult(ItemList recommendationList,
			Set<String> expectedSet) {
		precision10 = checkPrecision(10, recommendationList, expectedSet);
		recall10 = checkRecall(10, recommendationList, expectedSet);
	}

	boolean checkLikelihood(int n, ItemList recommendationList,
			Set<String> expectedSet) {

		// recommend the set...
		Set<Item> recommendationSet = recommendationList.recommend(n);

		for (Item iItem : recommendationSet) {
			if (expectedSet.contains(iItem.getElement()))
				return true;
		}
		return false;
	}

	double checkPrecision(int n, 
			ItemList recommendationList,
			Set<String> expectedSet) {
		int sum = 0;

		// recommend the set...
		Set<Item> recommendationSet = recommendationList.recommend(n);

		for (Item iItem : recommendationSet) {
			if (expectedSet.contains(iItem.getElement())) {
				sum++;
				sum_true++;
			}
			else {
				sum_false++;
			}
		}
		return ((double) sum) / ((double) recommendationSet.size());
	}

	double checkRecall(int n, ItemList recommendationList,
			Set<String> expectedSet) {
		int sum = 0;

		// recommend the set...
		Set<Item> recommendationSet = recommendationList.recommend(n);

		for (Item iItem : recommendationSet) {
			if (expectedSet.contains(iItem.getElement()))
				sum++;
		}
		return ((double) sum) / ((double) expectedSet.size());
	}

	double checkRecall(ItemList recommendationList, Set<String> currentSet) {
		int sum = 0;

		// recommend the set...
		Set<Item> recommendationSet = recommendationList
				.recommend(recommendationList.size());

		for (Item iItem : recommendationSet) {
			if (currentSet.contains(iItem.getElement()))
				sum++;
		}
		return ((double) sum) / ((double) currentSet.size());
	}

	public void print() {
		// if (Double.isNaN(recall))
		// System.out.println("Hi NaN!");

		System.out.println(" Precision@10:" + precision10 + "	: Recall@10:"
				+ recall10);
	}

	public void print(PrintStream outfile) {
		// if (Double.isNaN(recall))
		// System.out.println("Hi NaN!");

		outfile.println(" Precision@10:"
				+ precision10
				+ "	: Recall@10:"
				+ recall10
				+ "	: F-Measure:"
				+ ((precision10 == 0 && recall10 == 0) ? 0 : 2 * precision10
						* recall10 / (precision10 + recall10)));
	}

}
