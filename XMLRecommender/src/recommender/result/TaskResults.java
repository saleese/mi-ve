package recommender.result;

import java.io.PrintStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TaskResults {


	String file;
	List<Result> iList = new LinkedList<Result>();
	
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
	
	public List<Result> getiList() {
		return iList;
	}

	public void setiList(List<Result> iList) {
		this.iList = iList;
	}

	public boolean add(Result arg0) {
		return iList.add(arg0);
	}
	
	public boolean addAll(Collection<? extends Result> c) {
		return iList.addAll(c);
	}

	public void clear() {
		iList.clear();
	}

	public int size() {
		return iList.size();
	}
	
	public int calculateNth() { 
		int Nth = 0;
		
		for (Result result: iList) {
			Nth = Nth + result.getNth(); 
		}
		
		return Nth / iList.size();
	}
	
	public double calculatePrecision() { 
		double precision = 0.0;
		
		for (Result result: iList) {
			precision = precision + result.getPrecision(); 
		}
		
		return precision / iList.size();
	}
	
	public double calculateRecall() { 
		double recall = 0.0;
		
		for (Result result: iList) {
			recall = recall + result.getRecall(); 
		}
		
		return recall / iList.size();
	}
	
//	public double calculateLikelihood() { 
//		double likelihood = 0.0;
//				
//		for (Result result: iList) {
//			if (result.isLikelihood())
//				likelihood = likelihood + 1.0 ; 
//		}
//		
//		return likelihood / iList.size();
//	}
	
	public void print() {
		System.out.println(file);
		for (Result result: iList) {
			result.print();
		}
	}
	
	public void print(PrintStream outfile) {
		outfile.println("File: " + file + ":	"); // file name
		for (Result result: iList) {
			result.print(outfile);
//			break; // for initial recommendations...
		}
	}
}
