package recommender.result;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;


public class TotalResults {

	List<TaskResults> iList = new LinkedList<TaskResults>();
	
	public boolean add(TaskResults e) {
		return iList.add(e);
	}
	
	public void print() {
		for (TaskResults result: iList) {
			result.print();
		}
	}

	public void print(PrintStream outfile) {

		for (TaskResults result: iList) {
			result.print(outfile);
		}
	}
	
	public int size() {
		return iList.size();
	}
	
	public double sum() {
		double sum = 0.0;
		
		for (TaskResults results: iList) {
			sum = sum + results.size();
		}
		
		return sum;
	}

	public void clear() {
		iList.clear();
	}
	
	public int calculateNth () { 
		int  Nth = 0;
		int  sum = 0;
		
		for (TaskResults results: iList) {
			for (Result result: results.getiList()) {
				Nth = Nth + result.getNth(); 				
			}
			sum = sum + results.size();
		}
		
		return Nth / sum;
	}
	
	public double calculatePrecision() { 
		double precision = 0.0;
		double sum = 0.0;
		
		for (TaskResults results: iList) {
			for (Result result: results.getiList()) {
				precision = precision + result.getPrecision(); 				
			}
			sum = sum + results.size();
		}
		
		return precision / sum;
	}
	
	public double calculateRecall10() { 
		double recall10 = 0.0;
		double sum = 0.0;
				
		for (TaskResults results: iList) {
			for (Result result: results.getiList()) {
				if (Double.isNaN(result.getRecall())) {
//					System.out.println("Hi NaN!");
					sum--;
					continue;
				}
								
				recall10 = recall10 + result.getRecall(); 				
			}
			sum = sum + results.size();
		}
		
		return recall10 / sum;
	}
	
	public double calculateRecall() { 
		double recall = 0.0;
		double sum = 0.0;
				
		for (TaskResults results: iList) {
			for (Result result: results.getiList()) {
				if (Double.isNaN(result.getRecall())) {
//					System.out.println("Hi NaN!");
					sum--;
					continue;
				}
								
				recall = recall + result.getRecall(); 				
			}
			sum = sum + results.size();
		}
		
		return recall / sum;
	}
	

	public long calculateTrue () {		
		long  sum_true = 0;
		
		for (TaskResults results: iList) {
			for (Result result: results.getiList()) {
				sum_true = sum_true + result.getSum_true(); 				
			}			
		}
		
		return sum_true;
	}
	
	public long calculateFalse () {		
		long  sum_false = 0;
		
		for (TaskResults results: iList) {
			for (Result result: results.getiList()) {
				sum_false = sum_false + result.getSum_false(); 				
			}			
		}
		
		return sum_false;
	}
	
}
