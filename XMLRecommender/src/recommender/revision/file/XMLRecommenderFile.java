package recommender.revision.file;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import data.pairSet.RuleinSet;

import recommender.StructureHandlePrinter;
import recommender.result.TaskResults;
import recommender.result.TotalResults;

public abstract class XMLRecommenderFile {

	// problem: think about the appropriate data structure
	Vector<RuleinSet> accumulatedVector = new Vector<RuleinSet>();
	
	RuleinSet currentSet = new RuleinSet();
	
	public  PrintStream outfile;
	
	public XMLRecommenderFile(PrintStream tmpfile) {
		outfile = tmpfile;
	}
	
	// 테스트하는 부분임
	TaskResults currentResults = new TaskResults();	
	TotalResults totalResults = new TotalResults();
	int numQueries = 0;
	
	public void run(String directory) {

		try {
			// train data
			File file = new File(directory); 
			String[] list = file.list(new FilenameFilter() 
			{ 
				@Override 
				public boolean accept(File dir, String name)  
				{ 
					return name.endsWith(".xml"); 
				} 
			}); 
			Arrays.sort(list);

			for (int i = 0; i <list.length; i++) {

				// 파일 읽는 부분
				XMLInputFactory xmlif = XMLInputFactory.newInstance();
				XMLEventReader reader = xmlif.createXMLEventReader(new FileReader(directory + "/" + list[i]));
				XMLEvent event;

				while (reader.hasNext()) {
					event = reader.nextEvent();
					readDataRecord(event);
				}

//				if (list[i].contains("_E"))
//						outfile.print(list[i] +": ");
				currentResults.setFile(list[i]);
				recommendwithCurrentFile(directory, list[i]); // 파일로 추천 발생시키기.

				// 테스트 결과
				if (currentResults.size() > 0) {
					TaskResults tmpResults = new TaskResults();
					tmpResults.addAll(currentResults.getiList());
					tmpResults.setFile(currentResults.getFile());
					totalResults.add(tmpResults);
					currentResults.clear();
				}

				// 데이타 축적 부분임		    
				RuleinSet tmpSet = new RuleinSet();
				tmpSet.setFileName(list[i]);
				tmpSet.addAll(currentSet);
				accumulatedVector.add(tmpSet);
				currentSet.clear();
			}
			
			printResults2File();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void readDataRecord(XMLEvent event) {
		StructureHandlePrinter shp = new StructureHandlePrinter();


		if (event.isStartElement()) {
			StartElement element = (StartElement) event;

			if (element.getName().toString().equals("InteractionEvent")) {		        	  
				Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
				QName name1 = attribute1.getName();
				String value1 = attribute1.getValue();

				if (value1.equals("edit")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

					Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
					QName name2_1 = attribute2_1.getName();
					String value2_1 = attribute2_1.getValue();

					Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
					QName name2_2 = attribute2_2.getName();
					String value2_2 = attribute2_2.getValue();

					if (!value2_2.equals(value2_1)) {
						currentSet.addtoNext(shp.toFile(value2));
					}
					else {
						currentSet.addtoPrev(shp.toFile(value2));						
					}

				}
				else if (value1.equals("selection")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

					currentSet.addtoPrev(shp.toFile(value2));

				}
			}		        
		}
		if (event.isEndElement()) {
			EndElement element = (EndElement) event;
		}
		if (event.isCharacters()) {
			Characters characters = (Characters) event;
		}		
	}
	
	Set<String> getExpectedSet(Set<String> currentNextSet, Set<String> contextEditSet ,  Set<String> contextVisitSet) {
		Set<String> expectedSet = new TreeSet<String>(currentNextSet);
		expectedSet.removeAll(contextEditSet);
		expectedSet.removeAll(contextVisitSet);
		return expectedSet;
	}	
	
	void printVector() {
		int i = 0;
		for (RuleinSet tmpSet: accumulatedVector) {
			System.out.println(i++);
			for (String element: tmpSet.getNext()) {
				System.out.println(element);
			}
		}	
	}
	
	void printSet(Set<String> contextSet) {
		for (String structureHandler: contextSet) {
			System.out.println("- " + structureHandler);
		}
	}
	
	void printResults2File() {
//		totalResults.print(outfile); // 여기서 모든 결과 프린트 ...
		outfile.println("Precision@10 is: " + totalResults.calculatePrecision());
		outfile.println("Recall@10 is: " + totalResults.calculateRecall10());
		//			System.out.println("Recall is: " + totalResults.calculateRecall());
//		outfile.println(/* "Likelihood_1 is: " + */ totalResults.calculateLikelihood_C());
//		outfile.println(/* "Likelihood_2 is: " + */ totalResults.calculateLikelihood_P());
		outfile.println( "Feedback is: " + totalResults.sum() / (double) numQueries);
		outfile.println( "#True is: " + totalResults.calculateTrue());
		outfile.println( "#False is: " + totalResults.calculateFalse());
		outfile.println(/* "#recommendations is: " + */ (int) totalResults.sum());
		outfile.println(/* "#queries is: " + */ numQueries);
//		outfile.println(/* "#queries is: " + */ "*" +  totalResults.calculateNth());
	}
	
	abstract void recommendwithCurrentFile(String directory, String file) throws IOException;

}
