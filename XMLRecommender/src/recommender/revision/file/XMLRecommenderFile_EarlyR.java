package recommender.revision.file;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import recommender.ItemList;
import recommender.StructureHandlePrinter;
import recommender.result.Result;
import data.pairSet.RuleinSet;
import data.queue.StringQueue;

public class XMLRecommenderFile_EarlyR extends XMLRecommenderFile {
	
	public enum Tool {
	    MPI, ROSE 
	}	
	Tool tool = Tool.MPI;

	// 컨텍스트를 위한 자료 구조를 만든다.	
	int nVisitContext = 3;  // 파라미터 조정
	int nEditContext = 1;	// 파라미터 조정

	public XMLRecommenderFile_EarlyR(PrintStream tmpfile) {
		super(tmpfile);
	}

	void recommendwithCurrentFile(String directory, String file) throws IOException {
		if (checkFile4Recommendation(file)) return;

		int Nth = 0;

		StringQueue contextVisitQueue = new StringQueue(nVisitContext); // 파라미터 조정
		StringQueue contextEditQueue = new StringQueue(nEditContext); // 파라미터 조정
		boolean bRecommend = false;

		// 파일을 읽으면서 일정 컨텍스트가 형성되는지 확인한다.
		try {
			FileReader in = new FileReader(directory + "/" + file);
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			XMLEventReader reader;
			reader = xmlif.createXMLEventReader(in);	

			XMLEvent event;
			while (reader.hasNext()) {
				event = reader.nextEvent();
				StructureHandlePrinter shp = new StructureHandlePrinter();

				if (event.isStartElement()) {
					StartElement element = (StartElement) event;
					//	          System.out.println("Start Element: " + element.getName());

					if (element.getName().toString().equals("InteractionEvent")) {		        	  
						Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
						QName name1 = attribute1.getName();
						String value1 = attribute1.getValue();

						if (value1.equals("edit")) {		        	  
							Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
							QName name2 = attribute2.getName();
							String value2 = attribute2.getValue();

							Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
							QName name2_1 = attribute2_1.getName();
							String value2_1 = attribute2_1.getValue();

							Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
							QName name2_2 = attribute2_2.getName();
							String value2_2 = attribute2_2.getValue();

							if (!value2_2.equals(value2_1)) {
								//	System.out.println("edited-" + shp.toFile(value2));
								bRecommend = contextEditQueue.add(shp.toFile(value2));
								Nth++;
							}
							else {								
								//	System.out.println("viewed-" + shp.toFile(value2));
								if (tool == Tool.MPI) bRecommend = contextVisitQueue.add((shp.toFile(value2)));
								Nth++;
							}
						}
						else if (value1.equals("selection")) {		        	  
							Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
							QName name2 = attribute2.getName();
							String value2 = attribute2.getValue();

							//	System.out.println("viewed-" + shp.toFile(value2));
							if (tool == Tool.MPI) bRecommend = contextVisitQueue.add((shp.toFile(value2)));
							Nth++;
						}
					}		       
				}


				if (checkCondition(contextVisitQueue, contextEditQueue, bRecommend)) {	
					// making a recommendation
					numQueries++;	
					Set<String> contextVisitSet = contextVisitQueue.toSet();
					Set<String> contextEditSet = contextEditQueue.toSet();
					//					contextVisitQueue.clear();
					//					contextEditQueue.clear();

					//					outfile.println("#viewed: " + contextVisitSet.size() );
					Result result = recommend(contextVisitSet, contextEditSet, file, Nth);
					bRecommend = false;

					if (result != null) {// for first recommendation
						in.close();
						return;
					}				
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	boolean checkFile4Recommendation(String file) {
		if (!file.endsWith("_E.xml")) { // for early recommendations...
			return true;
		}
		else
			return false;		
	}

	boolean checkCondition(StringQueue contextVisitQueue, StringQueue contextEditQueue, boolean bRecommend) {
		if (((contextVisitQueue.size() >= nVisitContext) || (contextEditQueue.size() >= nEditContext)) && (bRecommend == true)) {
			return true;				
		}
		else {
			return false;
		}
	}

	// return: -1: no recommendation, 0: false, 1: true
	Result recommend(Set<String> contextVisitSet, Set<String> contextEditSet, String contextFile, int Nth) {
		//		printSet(contextVisitSet);
		System.out.println("Recommend!");

		ItemList recommendationList = new ItemList();

		double support = 0;		
		for (RuleinSet tmpSet: accumulatedVector) {
			String currentProj = contextFile.substring(0, contextFile.indexOf('-'));

			String pastFile = tmpSet.getFileName();
			String pastProj = pastFile.substring(0, pastFile.indexOf('-'));

			if (!currentProj.equals(pastProj)){
				continue;
			}

			if (tmpSet.containinPrev(contextVisitSet) || tmpSet.containinNext(contextEditSet)) {
				//					System.out.println("case #1:");
				System.out.println(pastFile);
				support = support + 1.0 ;

				for (String iStructureHandler: tmpSet.getNext()) {
					if (contextEditSet.contains(iStructureHandler)) 
						continue;
					else if (contextVisitSet.contains(iStructureHandler)) // 이건 잠시 질문에 대답하기 위해 넣은 것임 
						continue;
					else
						recommendationList.add(iStructureHandler);
				}
			}

			if (recommendationList.isEmpty()) {
				if (tmpSet.containinPrev(contextVisitSet)) {
					//					System.out.println("case #2:" + contextVisitSet.size());
					System.out.println(pastFile);
					support = support + 1.0 ; // error exists...

					for (String iStructureHandler: tmpSet.getNext()) {
						if (contextVisitSet.contains(iStructureHandler)) // 이건 잠시 질문에 대답하기 위해 넣은 것임 
							continue;
						else
							recommendationList.add(iStructureHandler);
					}
				}
			}
			if (recommendationList.isEmpty()) {
				if (tmpSet.containinNext(contextEditSet)) {
					//					System.out.println("case #3:" + contextEditSet.size());
					System.out.println(pastFile);
					support = support + 1.0 ; // error exists...

					for (String iStructureHandler: tmpSet.getNext()) {
						if (contextEditSet.contains(iStructureHandler)) 
							continue;
						else
							recommendationList.add(iStructureHandler);
					}
				}
			}

		}
		System.out.println();
		recommendationList.calculate_confidence(support);
		recommendationList.sort();
		//			recommendationList.remove_min_support(4);

		if (recommendationList.isEmpty()) return null;

		Result result = new Result();
		Set<String> expectedSet = getExpectedSet(currentSet.getNext(), contextEditSet, contextVisitSet);
		result.checkResult(recommendationList, expectedSet);
		result.setNth(Nth);
		
//		outfile.format("Nth: %5d" + "   : precision: %5.2f"  + "   : recall: %5.2f\n", Nth, result.getPrecision(), result.getRecall());
		
		if (contextFile.contains("280973-139683") ) {

			System.out.println(Nth);

			System.out.println("Visit Context:");
			for (String visitelement: contextVisitSet) {
				System.out.println(visitelement);
			}

			System.out.println("Edit Context:");
			for (String editelement: contextEditSet) {
				System.out.println(editelement);
			}

			result.print();
			System.out.println("recommended elements:");
			recommendationList.printRecommendedItems(10);
			System.out.println("expected elements:");
			printSet(expectedSet);

			System.exit(1);
		}

		outfile.format("Nth: %5d" + "   : precision: %5.2f"  + "   : recall: %5.2f\n", Nth, result.getPrecision(), result.getRecall());

		//			result.print(); // print...
		currentResults.add(result);

		recommendationList.clear();
		return result; // 수정해야 함
	}

}