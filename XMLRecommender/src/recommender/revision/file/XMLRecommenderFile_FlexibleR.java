package recommender.revision.file;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import recommender.ItemList;
import recommender.StructureHandlePrinter;
import recommender.result.Result;
import data.pairSet.RuleinSet;
import data.queue.StringQueue;

public class XMLRecommenderFile_FlexibleR extends XMLRecommenderFile {

	public enum Tool {
		ROSE, MPI
	}	
	Tool tool = Tool.MPI;

	public enum Combi {
		AND, OR, OnlyView, OnlyEdit 
	}	
	Combi combi = Combi.AND;

	// condition for context formation
	//	final int K = 0;
	int nVisitContext = 5; // 파라미터 조정
	int nEditContext = 5;	// 파라미터 조정

	public XMLRecommenderFile_FlexibleR(PrintStream tmpfile) {
		super(tmpfile);
	}

	void recommendwithCurrentFile(String directory, String file) {		
		StringQueue contextVisitQueue = new StringQueue(nVisitContext); 
		StringQueue contextEditQueue = new StringQueue(nEditContext); 		
		boolean bRecommend = false;
		int Nth = 0;

		// 파일을 읽으면서 일정 컨텍스트가 형성되는지 확인한다.
		try {
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			XMLEventReader reader;
			reader = xmlif.createXMLEventReader(new FileReader(directory + "/" + file));	

			XMLEvent event;
			while (reader.hasNext()) {
				event = reader.nextEvent();
				StructureHandlePrinter shp = new StructureHandlePrinter();

				if (event.isStartElement()) {
					StartElement element = (StartElement) event;
					if (element.getName().toString().equals("InteractionEvent")) {		        	  
						Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
						String value1 = attribute1.getValue();

						if (value1.equals("edit")) {		        	  
							Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
							String value2 = attribute2.getValue();

							Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
							String value2_1 = attribute2_1.getValue();

							Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
							String value2_2 = attribute2_2.getValue();

							if (!value2_2.equals(value2_1)) {
								//						        System.out.println("edited-" + shp.toFile(value2));
								if (nEditContext != 0) bRecommend = contextEditQueue.add(shp.toFile(value2));
								Nth++;
							}
							else {								
								//							    System.out.println("viewed-" + shp.toFile(value2));
								if (tool == Tool.MPI) {
									bRecommend = contextVisitQueue.add((shp.toFile(value2)));
								}
								Nth++;
							}
						}
						else if (value1.equals("selection")) {		        	  
							Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
							QName name2 = attribute2.getName();
							String value2 = attribute2.getValue();
							//							System.out.println("viewed-" + shp.toFile(value2));
							if (tool == Tool.MPI) {
								bRecommend = contextVisitQueue.add((shp.toFile(value2)));
							}
							Nth++;
						}
					}		       
				}

				if (checkCondition(contextVisitQueue, contextEditQueue, bRecommend)) {
					//				if (/* (contextVisitQueue.size() >= K)  || */ (contextEditQueue.size() >= nEditContext) && (bRecommend == true)) { // change the event positions...	
					numQueries++;	
					Set<String> contextVisitSet = contextVisitQueue.toSet();
					Set<String> contextEditSet = contextEditQueue.toSet();

					System.out.println("v:" + contextVisitSet.size());
					System.out.println("e:" + contextEditSet.size());

					Result result = recommend(contextVisitSet, contextEditSet, file, Nth);

					bRecommend = false;
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	boolean checkCondition(StringQueue contextVisitQueue, StringQueue contextEditQueue, boolean bRecommend) {
		if (combi == Combi.AND) { // ... this is for a hybrid edit + view... //
			if ((contextVisitQueue.size() >= nVisitContext) && (contextEditQueue.size() >= nEditContext) && (bRecommend == true)) { // MPI-E
				return true;
			}
		}
		else if (combi == Combi.OR) { // ... this is for only edit or view ... //
			if (((contextVisitQueue.size() >= nVisitContext) || (contextEditQueue.size() >= nEditContext)) && (bRecommend == true)) { // MPI-V
				return true;				
			}
		}
		else if (combi == Combi.OnlyView) { // ... this is for only edit or view ... //
			if ((contextVisitQueue.size() >= nVisitContext) && (bRecommend == true)) { // MPI-V
				return true;				
			}
		}
		else if (combi == Combi.OnlyEdit) { // ... this is for only edit or view ... //
			if ((contextEditQueue.size() >= nEditContext) && (bRecommend == true)) { // MPI-V
				return true;				
			}
		}
		return false; // other else
	}

	// making a recommendation						
	// return: -1: no recommendation, 0: false, 1: true 
	Result recommend(Set<String> contextVisitSet, Set<String> contextEditSet, String contextFile, int Nth) {
		//		printSet(contextVisitSet);
		ItemList recommendationList = new ItemList();

		double support = 0;		
		for (RuleinSet tmpSet: accumulatedVector) {
			String pastFile = tmpSet.getFileName();
			String pastProj = pastFile.substring(0, pastFile.indexOf('-'));

			String currentProj = contextFile.substring(0, contextFile.indexOf('-'));
			if (!currentProj.equals(pastProj)){
				continue;
			}

			// change the event positions... 
			if (((combi == Combi.AND) && (tmpSet.containinPrev(contextVisitSet) && tmpSet.containinNext(contextEditSet))) ||
			   (((combi == Combi.OnlyView) || (combi == Combi.OnlyEdit)) && (tmpSet.containinPrev(contextVisitSet) || tmpSet.containinNext(contextEditSet)))) { 
				//				System.out.println("File:" + tmpSet.getFileName());
				support = support + 1.0 ;
				for (String iStructureHandler: tmpSet.getNext()) {
					if (contextEditSet.contains(iStructureHandler)) 
						continue;
					else if (contextVisitSet.contains(iStructureHandler)) 
						continue; 
					else
						recommendationList.add(iStructureHandler);
				}
			}
			else if (combi == Combi.OR) {
				if ((contextVisitSet.size() > 0) && (contextEditSet.size() > 0)) {
					if ((tmpSet.containinPrev(contextVisitSet) && tmpSet.containinNext(contextEditSet))) {
						//				System.out.println("File:" + tmpSet.getFileName());
						support = support + 1.0 ;
						for (String iStructureHandler: tmpSet.getNext()) {
							if (contextEditSet.contains(iStructureHandler)) 
								continue;
							else if (contextVisitSet.contains(iStructureHandler)) 
								continue; 
							else
								recommendationList.add(iStructureHandler);
						}
					}					
				}
				else {
					if ((tmpSet.containinPrev(contextVisitSet) || tmpSet.containinNext(contextEditSet))) {
						//				System.out.println("File:" + tmpSet.getFileName());
						support = support + 1.0 ;
						for (String iStructureHandler: tmpSet.getNext()) {
							if (contextEditSet.contains(iStructureHandler)) 
								continue;
							else if (contextVisitSet.contains(iStructureHandler)) 
								continue; 
							else
								recommendationList.add(iStructureHandler);
						}
					}					
				}
			}			
		}
		recommendationList.calculate_confidence(support);
		recommendationList.sort();
		if (recommendationList.isEmpty()) return null;

		Result result = new Result();
		Set<String> expectedSet = getExpectedSet(currentSet.getNext(), contextEditSet, contextVisitSet);
		result.checkResult(recommendationList, expectedSet);	
		result.setNth(Nth);
		currentResults.add(result);

		recommendationList.clear();
		return result; // 수정해야 함
	}

}