package recommender.revision.hybrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.PrintStream;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import data.pairSet.RuleinSet;
import data.queue.StringQueue;

import recommender.Item;
import recommender.ItemList;
import recommender.StructureHandlePrinter;
import recommender.result.Result;
import recommender.result.TaskResults;
import recommender.result.TotalResults;


public class XMLRecommenderHybrid {

	// problem: think about the appropriate data structure
	Vector<RuleinSet> accumulatedVector = new Vector<RuleinSet>();
	RuleinSet currentSet = new RuleinSet();

	// 테스트하는 부분임
	TaskResults currentResults = new TaskResults();	
	TotalResults totalResults = new TotalResults();
	int numQueries = 0;

	public  FileOutputStream output;
	public  PrintStream outfile;

	public void run(String directory) {

		//must use a try/catch statement for it to work
		try
		{     
			output = new FileOutputStream("out.txt");
			outfile = new PrintStream(output);
		}        
		catch(Exception e)
		{
			System.out.println("Could not load file!");
		}

		try {
			// train data
			File file = new File(directory); 
			String[] list = file.list(new FilenameFilter() 
			{ 
				@Override 
				public boolean accept(File dir, String name)  
				{ 
					return name.endsWith(".xml"); 
				} 
			}); 


			for (int i = 0; i <list.length; i++) {

				// 파일 읽는 부분
				XMLInputFactory xmlif = XMLInputFactory.newInstance();
				XMLEventReader reader = xmlif.createXMLEventReader(new FileReader(directory + "/" + list[i]));
				XMLEvent event;

				while (reader.hasNext()) {
					event = reader.nextEvent();
					countEdit(event);
				}		

				System.out.println(list[i] +": ");
				recommendwithCurrentFile(directory, list[i]); // 파일로 추천 발생시키기.

				// 테스트 결과
				if (currentResults.size() > 0) {
					TaskResults tmpResults = new TaskResults();
					tmpResults.addAll(currentResults.getiList());
					totalResults.add(tmpResults);
					currentResults.clear();
				}

//				// 데이타 축적 부분임		    
				RuleinSet tmpSet = new RuleinSet();
				tmpSet.setFileName(list[i]);
				tmpSet.addAll(currentSet);
				accumulatedVector.add(tmpSet);
				currentSet.clear();
			}

//			totalResults.print(); // 여기서 모든 결과 프린트 ...
//			System.out.println("Precision1 is: " + totalResults.calculatePrecision1());
			System.out.println("Precision@10 is: " + totalResults.calculatePrecision());
			System.out.println("Recall@10 is: " + totalResults.calculateRecall10());
//			System.out.println("Recall is: " + totalResults.calculateRecall());
			System.out.println("Feedback is: " + totalResults.sum() / (double) numQueries);
			System.out.println("#recommendations is: " + (int) totalResults.sum());
			System.out.println("#queries is: " + numQueries);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void countEdit(XMLEvent event) {
		StructureHandlePrinter shp = new StructureHandlePrinter();


		if (event.isStartElement()) {
			StartElement element = (StartElement) event;

			if (element.getName().toString().equals("InteractionEvent")) {		        	  
				Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
				QName name1 = attribute1.getName();
				String value1 = attribute1.getValue();

				if (value1.equals("edit")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

					Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
					QName name2_1 = attribute2_1.getName();
					String value2_1 = attribute2_1.getValue();

					Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
					QName name2_2 = attribute2_2.getName();
					String value2_2 = attribute2_2.getValue();

					if (!value2_2.equals(value2_1)) {
						currentSet.addtoNext(shp.toElement(value2));
					}
					else {
						currentSet.addtoPrev(shp.toElement(value2));						
					}

				}
				else if (value1.equals("selection")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

					currentSet.addtoPrev(shp.toElement(value2));

				}
			}		        
		}
		if (event.isEndElement()) {
			EndElement element = (EndElement) event;
		}
		if (event.isCharacters()) {
			Characters characters = (Characters) event;
		}		
	}

	void printVector() {
		int i = 0;
		for (RuleinSet tmpSet: accumulatedVector) {
			System.out.println(i++);
			for (String element: tmpSet.getNext()) {
				System.out.println(element);
			}
		}	
	}

	void recommendwithCurrentFile(String directory, String file) {

		// 컨텍스트를 위한 자료 구조를 만든다.	
		int nVisitContext = 3;	// 파라미터 조정
		int nEditContext = 1;	// 파라미터 조정
		
		StringQueue contextVisitQueue = new StringQueue(nVisitContext); // 파라미터 조정
		StringQueue contextEditQueue = new StringQueue(nEditContext); // 파라미터 조정
		boolean bRecommend = false;

		// 파일을 읽으면서 일정 컨텍스트가 형성되는지 확인한다.
		try {
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			XMLEventReader reader;
			reader = xmlif.createXMLEventReader(new FileReader(directory + "/" + file));	

			XMLEvent event;
			while (reader.hasNext()) {
				event = reader.nextEvent();
				StructureHandlePrinter shp = new StructureHandlePrinter();

				if (event.isStartElement()) {
					StartElement element = (StartElement) event;
					//	          System.out.println("Start Element: " + element.getName());

					if (element.getName().toString().equals("InteractionEvent")) {		        	  
						Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
						QName name1 = attribute1.getName();
						String value1 = attribute1.getValue();

						if (value1.equals("edit")) {		        	  
							Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
							QName name2 = attribute2.getName();
							String value2 = attribute2.getValue();

							Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
							QName name2_1 = attribute2_1.getName();
							String value2_1 = attribute2_1.getValue();

							Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
							QName name2_2 = attribute2_2.getName();
							String value2_2 = attribute2_2.getValue();

							if (!value2_2.equals(value2_1)) {
								bRecommend = contextEditQueue.add(shp.toElement(value2));
							}
							else {
								bRecommend = contextVisitQueue.add(shp.toElement(value2));
							}

						}
						else if (value1.equals("selection")) {		        	  
							Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
							QName name2 = attribute2.getName();
							String value2 = attribute2.getValue();

							bRecommend = contextVisitQueue.add(shp.toElement(value2));
						}
					}		       
				}

//				if ((contextVisitQueue.size() < nVisitContext) || (bRecommend == false)){
//					// forming a context
//					continue;
//				}
//				if ((contextEditQueue.size() < nEditContext) || (bRecommend == false)){
//					// forming a context
//					continue;
//				}
//				else {
				if (((contextVisitQueue.size() >= nVisitContext) && (bRecommend == true)) || 
					((contextEditQueue.size() >= nEditContext) && (bRecommend == true))){	
					// making a recommendation			
					numQueries++;	
					Set<String> contextVisitSet = contextVisitQueue.toSet();
					Set<String> contextEditSet = contextEditQueue.toSet();
					
//					outfile.println("#viewed: " + contextVisitSet.size() );
					Result result = recommend(contextVisitSet, contextEditSet);
					bRecommend = false;
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	//	void recommendwithCurrentSet() {
	//		int iTrue =0, iFalse = 0;
	//		Set<String> contextSet = new LinkedHashSet<String>();
	//		int nContext = 1;	// 파라미터 조정		
	//
	//
	//		for (String structureHandler: currentSet.getPrev()) {
	//			contextSet.add(structureHandler);
	//			
	//			if (contextSet.size() < nContext) {
	//				// forming a context
	//				continue;
	//			}
	//			else {
	//				// making a recommendation			
	//				numQueries++;			
	//				Result result = recommend(contextSet);
	//				contextSet.clear();
	//			}
	//		}
	//	}

	// return: -1: no recommendation, 0: false, 1: true
	Result recommend(Set<String> contextVisitSet, Set<String> contextEditSet) {
		ItemList recommendationList = new ItemList();

		double support = 0;		
		for (RuleinSet tmpSet: accumulatedVector) {
			if (tmpSet.containinPrev(contextVisitSet) && tmpSet.containinNext(contextEditSet)) {
//				System.out.println("File:" + tmpSet.getFileName());
				support = support + 1.0 ;

				for (String iStructureHandler: tmpSet.getNext()) {
					if (contextEditSet.contains(iStructureHandler)) 
						continue;
					else
						recommendationList.add(iStructureHandler);
				}
			}
		}
		recommendationList.calculate_confidence(support);
		recommendationList.sort();

		if (recommendationList.isEmpty()) return null;

		Result result = new Result();
		Set<String> expectedSet = getExpectedSet(currentSet.getNext(), contextEditSet);
		result.checkResult(recommendationList, expectedSet);
		currentResults.add(result);

		recommendationList.clear();
		return result; // 수정해야 함
	}

	Set<String> getExpectedSet(Set<String> currentNextSet, Set<String> contextEditSet) {
	    Set<String> expectedSet = new TreeSet<String>(currentNextSet);
	    expectedSet.removeAll(contextEditSet);
	    return expectedSet;
	}
	
	void printCurrentSet() {
		for (String structureHandler: currentSet.getNext()) {
			System.out.println(structureHandler);
		}
	}

}