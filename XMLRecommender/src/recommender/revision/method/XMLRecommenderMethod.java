package recommender.revision.method;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import data.pairSet.RuleinSet;
import data.queue.StringQueue;

import recommender.Item;
import recommender.ItemList;
import recommender.StructureHandlePrinter;
import recommender.result.Result;
import recommender.result.TaskResults;
import recommender.result.TotalResults;


public class XMLRecommenderMethod {

	// problem: think about the appropriate data structure
	Vector<RuleinSet> accumulatedVector = new Vector<RuleinSet>();
	RuleinSet currentSet = new RuleinSet();

	// 테스트하는 부분임
	TaskResults currentResults = new TaskResults();	
	TotalResults totalResults = new TotalResults();
	int numQueries = 0;
	
	PrintStream outfile;
	
	public XMLRecommenderMethod(PrintStream outfile) {
		this.outfile = outfile;
	}

	public void run(String directory, int nContext) {

		try {
			// train data
			File file = new File(directory); 
			String[] list = file.list(new FilenameFilter() 
			{ 
				@Override 
				public boolean accept(File dir, String name)  
				{ 
					return name.endsWith(".xml"); 
				} 
			}); 


			for (int i = 0; i <list.length; i++) {

				// 파일 읽는 부분
				XMLInputFactory xmlif = XMLInputFactory.newInstance();
				XMLEventReader reader = xmlif.createXMLEventReader(new FileReader(directory + "/" + list[i]));
				XMLEvent event;

//				int count = 0;
				while (reader.hasNext()) {
//					System.out.println(count++);
					event = reader.nextEvent();
					countEdit(event);
				}		
				// 추천 수행
				System.out.println(list[i] +": ");	
				currentResults.setFile(list[i]);
				recommendwithCurrentFile(directory, list[i], nContext); // 파일로 추천 발생시키기.

				// 테스트 결과
				if (currentResults.size() > 0) {
					TaskResults tmpResults = new TaskResults();
					tmpResults.addAll(currentResults.getiList());
					tmpResults.setFile(currentResults.getFile());
					totalResults.add(tmpResults);
					currentResults.clear();
				}

//				// 데이타 축적 부분임		    
				RuleinSet tmpSet = new RuleinSet();
				tmpSet.setFileName(list[i]);
				tmpSet.addAll(currentSet);
				accumulatedVector.add(tmpSet);
				currentSet.clear();
			}

			totalResults.print(outfile); // 여기서 모든 결과 프린트 ...
//			outfile.println("n is: " + nContext);
			outfile.println(/*"Precision@10 is: " + */ totalResults.calculatePrecision());
			outfile.println(/*"Recall@10 is: " + */ totalResults.calculateRecall10());
			outfile.println(/*"Feedback is: " + */totalResults.sum() / (double) numQueries);
			outfile.println(/*"#recommendations is: " + */ (int) totalResults.sum());
			outfile.println(/* "#queries is: " + */ numQueries);
			outfile.println();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void countEdit(XMLEvent event) {
		StructureHandlePrinter shp = new StructureHandlePrinter();


		if (event.isStartElement()) {
			StartElement element = (StartElement) event;

			if (element.getName().toString().equals("InteractionEvent")) {		        	  
				Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
				QName name1 = attribute1.getName();
				String value1 = attribute1.getValue();

				if (value1.equals("edit")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

					Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
					QName name2_1 = attribute2_1.getName();
					String value2_1 = attribute2_1.getValue();

					Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
					QName name2_2 = attribute2_2.getName();
					String value2_2 = attribute2_2.getValue();
					
//					System.out.println(value2_2);

					if (!value2_2.equals(value2_1)) {
						currentSet.addtoNext((shp.toElement(value2)));
					}
					else {
						currentSet.addtoPrev((shp.toElement(value2)));						
					}

				}
				else if (value1.equals("selection")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();
		
					currentSet.addtoPrev((shp.toElement(value2)));

				}
			}		        
		}
		if (event.isEndElement()) {
			EndElement element = (EndElement) event;
		}
		if (event.isCharacters()) {
			Characters characters = (Characters) event;
		}		
	}

	void printVector() {
		int i = 0;
		for (RuleinSet tmpSet: accumulatedVector) {
			System.out.println(i++);
			for (String element: tmpSet.getNext()) {
				System.out.println(element);
			}
		}	
	}

	void recommendwithCurrentFile(String directory, String file, int nContext) {
		
//		Set<String> visitedSet = new HashSet<String>();

		// 컨텍스트를 위한 자료 구조를 만든다.	
		int nVisitContext = 3; // nContext;	// 파라미터 조정
		int nEditContext = nContext;	// 파라미터 조정
		
		StringQueue contextVisitQueue = new StringQueue(nVisitContext); // 파라미터 조정
		StringQueue contextEditQueue = new StringQueue(nEditContext); // 파라미터 조정
		boolean bRecommend = false;

		// 파일을 읽으면서 일정 컨텍스트가 형성되는지 확인한다.
		try {
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			XMLEventReader reader;
			reader = xmlif.createXMLEventReader(new FileReader(directory + "/" + file));	

			XMLEvent event;
			while (reader.hasNext()) {
				event = reader.nextEvent();
				StructureHandlePrinter shp = new StructureHandlePrinter();

				if (event.isStartElement()) {
					StartElement element = (StartElement) event;
					//	          System.out.println("Start Element: " + element.getName());

					if (element.getName().toString().equals("InteractionEvent")) {		        	  
						Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
						QName name1 = attribute1.getName();
						String value1 = attribute1.getValue();

						if (value1.equals("edit")) {		        	  
							Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
							QName name2 = attribute2.getName();
							String value2 = attribute2.getValue();

							Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
							QName name2_1 = attribute2_1.getName();
							String value2_1 = attribute2_1.getValue();

							Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
							QName name2_2 = attribute2_2.getName();
							String value2_2 = attribute2_2.getValue();

							if (!value2_2.equals(value2_1)) {
								bRecommend = contextEditQueue.add(shp.toElement(value2));
							}
							else {
//								if (!visitedSet.contains(shp.toElement(value2))) { // added for this test
//									bRecommend = contextVisitQueue.add(shp.toElement(value2));
//									visitedSet.add(shp.toElement(value2));
//								}
							}

						}
						else if (value1.equals("selection")) {		        	  
							Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
							QName name2 = attribute2.getName();
							String value2 = attribute2.getValue();
//							if (!visitedSet.contains(shp.toElement(value2))) { // added for this test
//								bRecommend = contextVisitQueue.add((shp.toElement(value2)));
//								visitedSet.add(shp.toElement(value2));
//							}
						}
					}		       
				}
				// ... this is for a hybrid edit + view... //
//				if ((contextVisitQueue.size() < nVisitContext) || (bRecommend == false)){
//					// forming a context
//					continue;
//				}
//				if ((contextEditQueue.size() < nEditContext) || (bRecommend == false)){
//					// forming a context
//					continue;
//				}
//				else { // from here 2 lines for only edit or for only view
				if (((contextVisitQueue.size() >= nVisitContext) && (bRecommend == true)) || 
					((contextEditQueue.size() >= nEditContext) && (bRecommend == true))){	
					// making a recommendation			
					numQueries++;	
					Set<String> contextVisitSet = contextVisitQueue.toSet();
					Set<String> contextEditSet = contextEditQueue.toSet();
					
//					outfile.println("#viewed: " + contextVisitSet.size() );
					Result result = recommend(contextVisitSet, contextEditSet, file);
					bRecommend = false;
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// return: -1: no recommendation, 0: false, 1: true
	Result recommend(Set<String> contextVisitSet, Set<String> contextEditSet, String contextFile) {
		ItemList recommendationList = new ItemList();

		double support = 0;		
		for (RuleinSet tmpSet: accumulatedVector) {
			String currentProj = contextFile.substring(0, contextFile.indexOf('-'));
			
			String pastFile = tmpSet.getFileName();
			String pastProj = pastFile.substring(0, pastFile.indexOf('-'));
			
			if (!currentProj.equals(pastProj)){
					continue;
			}
			
			if (tmpSet.containinPrev(contextVisitSet) && tmpSet.containinNext(contextEditSet)) {
//				System.out.println("File:" + tmpSet.getFileName());
				support = support + 1.0 ;

				for (String iStructureHandler: tmpSet.getNext()) {
					if (contextEditSet.contains(iStructureHandler)) 
						continue;
					else if (contextVisitSet.contains(iStructureHandler)) // 이건 잠시 질문에 대답하기 위해 넣은 것임 
						continue;
					else
						recommendationList.add(iStructureHandler);
				}
			}
		}
		recommendationList.calculate_confidence(support);
		recommendationList.sort();

		if (recommendationList.isEmpty()) return null;

		Result result = new Result();
		
		Set<String> expectedSet = getExpectedSet(currentSet.getNext(), contextEditSet , contextVisitSet ); // 해당 view 엘리먼트도 제거
		result.checkResult(recommendationList, expectedSet);
//		if (contextFile.contains("221583-91738")) {
//			System.out.println("### Recommendation ###");			
//			recommendationList.print(1);
//			System.out.println("### Expected Ones ###");
//			this.printCurrentSet();
//			System.exit(1);
//		}
		
		currentResults.add(result);

		recommendationList.clear();
		return result; // 수정해야 함
	}

	Set<String> getExpectedSet(Set<String> currentNextSet, Set<String> contextEditSet , Set<String> contextVisitSet ) {
	    Set<String> expectedSet = new TreeSet<String>(currentNextSet);
	    expectedSet.removeAll(contextEditSet);
	    expectedSet.removeAll(contextVisitSet);
	    return expectedSet;
	}
	
	void printCurrentSet() {
		for (String structureHandler: currentSet.getNext()) {
			System.out.println(structureHandler);
		}
	}

}