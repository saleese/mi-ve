package tester;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class CompareTwoFiles {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<Value> array1 = new LinkedList<Value>();
		LinkedList<Value> array2 = new LinkedList<Value>();

		try {
			// //////////////////////////////////////////////////////////////
			BufferedReader in1 = new BufferedReader(new FileReader(
					"PDE_MPI.txt"));
			BufferedReader in2 = new BufferedReader(new FileReader(
					"PDE_ROSE.txt"));
			String s1, s2;
			while ((s1 = in1.readLine()) != null) {

				StringTokenizer tokens = new StringTokenizer(s1, ":");
				if (tokens.countTokens() < 7)
					continue;

				Value value1 = new Value();
				value1.setValues(tokens);
				array1.add(value1);
			}
			in1.close();

			while ((s2 = in2.readLine()) != null) {

				StringTokenizer tokens = new StringTokenizer(s2, ":");
				if (tokens.countTokens() < 7)
					continue;

				Value value2 = new Value();
				value2.setValues(tokens);
				array2.add(value2);
			}
			in1.close();

			for (int i = 1; i < array1.size(); i++) {
				if ((array1.get(i).getN() < array2.get(i).getN())
						&& (array1.get(i).getPrecision() > array2.get(i)
								.getPrecision())
						&& (array1.get(i).getRecall() >= array2.get(i)
								.getRecall())) {
					System.out.print(array1.get(i).getFilename() + " ");
					System.out.print("{ " + array1.get(i).getN() + ", "
							+ array1.get(i).getPrecision() + ", "
							+ array1.get(i).getRecall() + "} ");
					System.out.println("{ " + array2.get(i).getN() + ", "
							+ array2.get(i).getPrecision() + ", "
							+ array2.get(i).getRecall() + "} ");
				}
			}

			// //////////////////////////////////////////////////////////////
		} catch (IOException e) {
			System.err.println(e); // 에러가 있다면 메시지 출력
			System.exit(1);
		}

	}

}
