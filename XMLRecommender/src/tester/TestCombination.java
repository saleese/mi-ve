package tester;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class TestCombination {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// input
		Set<String> visitedSet = new HashSet<String>();
		visitedSet.add("Kennedy");
		visitedSet.add("Johnson");
		visitedSet.add("Nixon");
		visitedSet.add("Ford");
		visitedSet.add("Ah");		
		
		// output
		Vector<Set<String>> v = new Vector();		
		v = combination(visitedSet.toArray());
		
		for (Set<String> vSet: v) {
			for (String element: vSet) {
				System.out.print(element + ", ");
			}
			System.out.println();
		}
	}
	
	static Vector combination(Object[] list) {
		Vector<Set<String>> v = new Vector();		
		for (int a = 0; a < list.length; a++) {
			for (int b = a + 1; b < list.length; b++) {
				for (int c = b + 1; c < list.length; c++) {					
					Set<String> vSet = new HashSet<String>();	
					vSet.add((String) list[a]);
					vSet.add((String) list[b]);
					vSet.add((String) list[c]);					
					v.add(vSet);
				}
			}			
		}
		return v;
	}

}
