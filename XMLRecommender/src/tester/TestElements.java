package tester;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.HashMap;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import recommender.StructureHandlePrinter;

public class TestElements {

	static String directory = "E:/Sample";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// train data
			File file = new File(directory); 
			String[] list = file.list(new FilenameFilter() 
			{ 
				@Override 
				public boolean accept(File dir, String name)  
				{ 
					return name.endsWith(".xml"); 
				} 
			}); 

			for (int i = 0; i <list.length; i++) {

				// 테스트하는 부분임
				String date = null;
				
					// 파일 읽는 부분
					System.out.println(directory + "/" + list[i]);
					XMLInputFactory xmlif = XMLInputFactory.newInstance();
					FileReader filereader = new FileReader(directory + "/" + list[i]);
					XMLEventReader reader = xmlif.createXMLEventReader(filereader);
					XMLEvent event;
					
					while (reader.hasNext()) {
						event = reader.nextEvent();
						countEdit(event);
						if (date != null)
							break;
					}
					reader.close();
					filereader.close();										
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static void countEdit(XMLEvent event) {
		StructureHandlePrinter shp = new StructureHandlePrinter();


		if (event.isStartElement()) {
			StartElement element = (StartElement) event;
			//	          System.out.println("Start Element: " + element.getName());

			if (element.getName().toString().equals("InteractionEvent")) {
				Attribute attribute0 = element.getAttributeByName(new QName("EndDate"));
				QName name0 = attribute0.getName();
				String value0 = attribute0.getValue();				
				
				
				Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
				QName name1 = attribute1.getName();
				String value1 = attribute1.getValue();

				if (value1.equals("edit")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

					Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
					QName name2_1 = attribute2_1.getName();
					String value2_1 = attribute2_1.getValue();

					Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
					QName name2_2 = attribute2_2.getName();
					String value2_2 = attribute2_2.getValue();

					if (!value2_2.equals(value2_1)) {
						System.out.println(shp.toElement(value2));
//						System.out.println(value0 + ", edit, " + shp.toElement(value2));
					}
					else {
//						System.out.println(value0 + ", select, " + shp.toElement(value2));			
					}
						
				}
				else if (value1.equals("selection")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

//					System.out.println(value0 + ", select, " + shp.toElement(value2));		
				}
			}		        
		}
		if (event.isEndElement()) {
			EndElement element = (EndElement) event;
		}
		if (event.isCharacters()) {
			Characters characters = (Characters) event;
		}	
	}
}
