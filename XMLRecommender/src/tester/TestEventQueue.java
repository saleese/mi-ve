package tester;

import data.queue.EventQueue;

public class TestEventQueue {
	

	
	public static void main(String[] args) {
		EventQueue eventQueue = new EventQueue(3);
		
		eventQueue.add("a", "visit");
		eventQueue.add("b", "visit");
		eventQueue.add("c", "visit");
		eventQueue.add("d", "visit");
		eventQueue.add("e", "visit");
		
		eventQueue.print();
		
	}
	

}
