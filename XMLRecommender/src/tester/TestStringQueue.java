package tester;

import data.queue.StringQueue;

public class TestStringQueue {
	
	public static void main(String[] args) {
		StringQueue stringQueue = new StringQueue(3);
		
		stringQueue.add("a");
		stringQueue.add("b");
		stringQueue.add("c");
		stringQueue.add("d");
		stringQueue.add("c");
		
		stringQueue.print();		
	}	
}
