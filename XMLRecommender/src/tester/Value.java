package tester;

import java.util.StringTokenizer;

public class Value {
	
	public float getPrecision() {
		return precision;
	}
	public void setPrecision(float precision) {
		this.precision = precision;
	}
	public float getRecall() {
		return recall;
	}
	public void setRecall(float recall) {
		this.recall = recall;
	}
	public int getN() {
		return n;
	}
	public void setN(int n) {
		this.n = n;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	private String filename;
	private float precision;
	private float recall;
	private int n;	
	
	public void setValues(StringTokenizer tokens) {
		String filename = tokens.nextToken();
		  if (filename.contains("xml")) {
			  this.setFilename(filename);
		  }
		  else {
			  return;
		  }
		  
		  String Nth = tokens.nextToken();
		  if (Nth.contains("Nth")) {
			  int n = Integer.parseInt(tokens.nextToken().trim());
			  this.setN(n);
		  }
		  else {
			  return;
		  }
		  
		  String precision = tokens.nextToken();
		  if (precision.contains("precision")) {
			  float p = Float.parseFloat(tokens.nextToken().trim());
			  this.setPrecision(p);
		  }
		  else {
			  return;
		  }
		  
		  String recall = tokens.nextToken();
		  if (recall.contains("recall")) {
			  float r = Float.parseFloat(tokens.nextToken().trim());
			  this.setRecall(r);
		  }
		  else {
			  return;
		  }
		
	}

}
