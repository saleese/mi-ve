package util.rename;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import recommender.StructureHandlePrinter;

public class PrintFiles {

	static String directory = "D:/MylynData/2011-07-30-mylyn-edit/a";
	//	static String directory2Move = "E:/2011-07-30-mylyn/productMylyn";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// train data
			File file = new File(directory); 
			String[] list = file.list(new FilenameFilter() 
			{ 
				@Override 
				public boolean accept(File dir, String name)  
				{ 
					return name.endsWith(".xml"); 
				} 
			});

			for (int i = 0; i <list.length; i++) {
				System.out.println(list[i]);
			}		

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
