package util.rename;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import recommender.StructureHandlePrinter;

public class RenameFile {

	static String inputDirectory = "D:/MylynData/2011-07-30-mylyn-edit/a";
	//	static String directory2Move = "E:/2011-07-30-mylyn/productMylyn";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// train data
			File file = new File(inputDirectory); 
			String[] list = file.list(new FilenameFilter() 
			{ 
				@Override 
				public boolean accept(File dir, String name)  
				{ 
					return name.endsWith(".xml"); 
				} 
			});

			for (int i = 0; i <list.length; i++) {
				System.out.println(list[i]);
				
				renameFile(inputDirectory + "/" + list[i]);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static void renameFile(String infile) throws IOException {
	    File inputFile = new File(infile);
	    String newFilename = infile.replace(".xml", "_E.xml");
	    File outputFile = new File(newFilename);
	    
	    System.out.println("hello1");
	    if(outputFile.exists()) {
	    	return;
	    }

	    // Rename file (or directory)
	    boolean success = inputFile.renameTo(outputFile);
	    if (!success) {
	        // File was not successfully renamed
		    System.out.println("no success");
	    }
	    System.out.println("hello2");
	}
}
