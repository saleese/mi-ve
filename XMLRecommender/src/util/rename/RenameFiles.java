package util.rename;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import recommender.StructureHandlePrinter;

public class RenameFiles {

	static String directory = "E:/2011-07-30-mylyn/mylyn";
	static String directory2Move = "E:/2011-07-30-mylyn/productMylyn";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// train data
			File file = new File(directory); 
			String[] list = file.list(new FilenameFilter() 
			{ 
				@Override 
				public boolean accept(File dir, String name)  
				{ 
					return name.endsWith(".xml"); 
				} 
			});

			// read text file;
			Hashtable<String,String> id_product_table = new Hashtable<String,String>();

			try {
				BufferedReader in = new BufferedReader(new FileReader("E:/2011-07-30-mylyn/bugId_product.log"));
				String line;

				while ((line = in.readLine()) != null) {
					String[] id_product = line.split(",");
					id_product_table.put(id_product[0], id_product[1]);
				}
				in.close();

				for (int i = 0; i <list.length; i++) {

					// get a project name
					String[] bug_id_file = list[i].split("-.");
					String project = id_product_table.get(bug_id_file[0]);					
					if (project.contains(" "))
						project = project.substring(0, project.indexOf(" "));

					// move a file
					// File (or directory) to be moved
					File file1 = new File(directory + "/" + list[i]);

					// Destination directory
					File dir = new File(directory2Move);

					// Move file to new directory
					boolean success = file1.renameTo(new File(dir, project + "-" + list[i]));

					if (!success) {
						System.out.println("unsuccessful");
					}
					else {
						System.out.println("successful");					
					}		

				}		

			} catch (IOException e) {
				System.err.println(e); // 에러가 있다면 메시지 출력
				System.exit(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
