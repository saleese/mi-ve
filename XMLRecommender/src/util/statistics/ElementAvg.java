package util.statistics;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.Vector;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import recommender.StructureHandlePrinter;
import data.pairSet.RuleinSet;

public class ElementAvg {
	
	Vector<RuleinSet> accumulatedVector = new Vector<RuleinSet>();
	RuleinSet currentSet = new RuleinSet();

//	int sum = 0;
	int TraceNo = 0;

	public void scanfile(String trainDirectory) {
		try {
			// train data
			File file = new File(trainDirectory); 
			String[] list = file.list(new FilenameFilter() 
			{ 
				@Override 
				public boolean accept(File dir, String name)  
				{ 
					return name.endsWith(".xml"); 
				} 
			}); 

			for (int i = 0; i < list.length; i++) {
				//					System.out.println(list[i] + ": " );
				TraceNo++;
				scanfile(trainDirectory, list[i]);
				
//				// 데이타 축적 부분임		    
				RuleinSet tmpSet = new RuleinSet();
				tmpSet.setFileName(list[i]);
				tmpSet.addAll(currentSet);
				accumulatedVector.add(tmpSet);
				currentSet.clear();
			}
			
			System.out.println("File Num:" + TraceNo);
			
			int n=0; 
			double nView=0, nEdit=0;
			for (RuleinSet iSet: accumulatedVector) {
				n++;
				nView = nView + iSet.getPrev().size();
				nEdit = nEdit + iSet.getNext().size();
			}
			System.out.println("viewed: " +  nView / n + "  edited: " + nEdit / n);			
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public void scanfile(String trainDirectory, String file) {
		try {
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			XMLEventReader reader = xmlif.createXMLEventReader(new FileReader(trainDirectory + "/" + file));
			XMLEvent event;
			while (reader.hasNext()) {
				event = reader.nextEvent();
				countEdit(event);
			}
			
			System.out.println("viewed: " + currentSet.getPrev().size() + "  edited: " + currentSet.getNext().size());
			
//						System.out.println(file + ": " + elementManager.size());
//						System.out.println(file + ": " + elementManager.countJava());
//						sum = sum + elementManager.size();
//						elementManager.clear();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	void countEdit(XMLEvent event) {
		StructureHandlePrinter shp = new StructureHandlePrinter();

		if (event.isStartElement()) {
			StartElement element = (StartElement) event;
			//	          System.out.println("Start Element: " + element.getName());

			if (element.getName().toString().equals("InteractionEvent")) {		        	  
				Attribute attribute1 = element.getAttributeByName(new QName("Kind"));
				QName name1 = attribute1.getName();
				String value1 = attribute1.getValue();
				
				if (value1.equals("edit")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

					Attribute attribute2_1 = element.getAttributeByName(new QName("StartDate"));
					QName name2_1 = attribute2_1.getName();
					String value2_1 = attribute2_1.getValue();

					Attribute attribute2_2 = element.getAttributeByName(new QName("EndDate"));
					QName name2_2 = attribute2_2.getName();
					String value2_2 = attribute2_2.getValue();

					if (!value2_2.equals(value2_1)) {
						currentSet.addtoNext((shp.toElement(value2)));
					}
					else {
						currentSet.addtoPrev((shp.toElement(value2)));						
					}

				}
				else if (value1.equals("selection")) {		        	  
					Attribute attribute2 = element.getAttributeByName(new QName("StructureHandle"));
					QName name2 = attribute2.getName();
					String value2 = attribute2.getValue();

					currentSet.addtoPrev((shp.toElement(value2)));	
				}
			}		        
		}
	
	}

}
